# PFCLS
Pierre-Feuille-Ciseaux-Lézard-Spock (PFCLS) est un jeu inventé dans la série The Big Bang Theory et un dérivé du célèbre jeu Pierre-Feuille-Ciseaux.

# Install

**Dev mode** (display on localhost:3000 / debug on localhost:9229) :
```npm run watch```

**Production build** :
```npm run build```

# Tools

### Front-side
- **React** as UI framework.
- **Redux** as state manager.

### Server-side
- **Express** as Node web application framework.
- **Socket.IO** as real-time application framework.
- **Helmet** as Express security tool.

### Development
- **Babel and plugins** as transpiler.
- **Babel CLI** as command line compiler.
- **Webpack** as modules bundler.
- **PostCSSNext** as next generation CSS transpiler.
- **Eslint and AirBnb config** as code checker.
- **Why Did You Update** as React useless re-rendering checker.
- **Nodemon** as Node serve files watcher.

... and more

# Algorithm
- Le lézard - empoisonne - Spock.
- Le lézard - mange - la feuille.
- Spock - casse - les ciseaux.
- Spock - vaporise - la pierre.
- Les ciseaux - décapite - le lézard.
- Les ciseaux - coupe - la feuille.
- La feuille - recouvre - la pierre.
- La feuille - discrédite - Spock.
- La pierre - écrase - le lézard.
- La pierre - émousse - les ciseaux.

# Ressources
- [React and Express app example](https://blog.codingbox.io/react-for-beginners-part-1-setting-up-repository-babel-express-web-server-webpack-a3a90cc05d1e)
- [Express and Socket.io app example ](https://code.tutsplus.com/tutorials/real-time-chat-with-nodejs-socketio-and-expressjs--net-31708)
- [Socket.io cheatsheet](https://gist.github.com/alexpchin/3f257d0bb813e2c8c476)
- [Node deployment](https://blog.risingstack.com/node-hero-deploy-node-js-heroku-docker/)
- [Production ready with Webpack and Express](https://alejandronapoles.com/2016/09/29/simple-production-environment-with-webpack-and-express/)
- [Chrome Dev tools on Node Server](https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27)
- [How to Lazy Load features using React and Webpack](https://codeburst.io/how-to-lazy-load-features-using-react-and-webpack-557f559ccde7)