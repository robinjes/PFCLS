// Librairies
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
// Components
import AppCtn from 'components/environments/App';
// Store
import store from 'store';
// Styles
import 'styles/common.css';

// Render
ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <AppCtn />
    </BrowserRouter>
  </Provider>
), document.getElementById('container'));
