/* Publics */

// Application
const setOnlineStatus = online => ({
  type: 'SET_ONLINE_STATUS',
  online
});

// Room
const addChallenger = challenger => ({
  type: 'ADD_CHALLENGER',
  challenger
});
const removeChallenger = id => ({
  type: 'REMOVE_CHALLENGER',
  id
});
const setChallengers = challengers => ({
  type: 'SET_CHALLENGERS',
  challengers
});

// Board
const setOpponent = id => ({
  type: 'SET_OPPONENT',
  id
});

// User
const setUser = user => ({
  type: 'SET_USER',
  user
});

export {
  setUser,
  setOpponent,
  setOnlineStatus,
  addChallenger,
  removeChallenger,
  setChallengers
};
