// Librairies
import React from 'react';
import PropTypes from 'prop-types';
// Styles
import './style.css';

const Button = ({ link, children, onClick, className }) => (
  link ?
    <a className={className ? `${className} button` : 'button'} href={link}>{children}</a> :
    <button className={className ? `${className} button` : 'button'} onClick={onClick}>{children}</button>
);

Button.propTypes = {
  className: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  link: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  onClick: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.element
  ]),
};

Button.defaultProps = {
  className: null,
  link: '',
  children: '',
  onClick: () => {}
};


export default Button;
