// Librairies
import React from 'react';
import PropTypes from 'prop-types';
// Styles
import './style.css';

const Field = props => (
  <div className={`${props.className ? props.className + ' ' : ''} field`}>
    {/* eslint-disable jsx-a11y/label-has-for */}
    {props.label !== '' &&
      <label htmlFor={props.id}>{props.label} :</label>
    }
    {/* eslint-enable jsx-a11y/label-has-for */}

    {props.placeholder && props.placeholder !== '' &&
      <input
        id={props.id}
        type={props.type}
        placeholder={props.placeholder}
        {...props}
      />
    }
    {!props.placeholder || props.placeholder === '' &&
      <input
        id={props.id}
        type={props.type}
        {...props}
      />
    }
    {/* eslint-enable jsx-a11y/label-has-for */}
  </div>
);

Field.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string,
  type: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string
};

Field.defaultProps = {
  className: '',
  type: 'text',
  label: '',
  placeholder: ''
};

export default Field;
