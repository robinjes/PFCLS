// Librairies
import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
// Components
import Header from 'javascripts/components/modules/Header';
import Room from 'javascripts/components/modules/Room';
import NotFound from 'javascripts/components/modules/NotFound';
// Styles
import './style.css';
// Why did you update
/* if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line no-unused-vars,react/no-deprecated
  let createClass = React.createClass;
  Object.defineProperty(React, 'createClass', {
    set: (nextCreateClass) => {
      createClass = nextCreateClass;
    },
  });
  // eslint-disable-next-line global-require
  const { whyDidYouUpdate } = require('why-did-you-update');
  whyDidYouUpdate(React);
} */

// Render
const App = () => (
  <div id="app">
    <Route path="*" component={Header} />
    <div className="main">
      <Switch>
        <Route exact path="/" component={Room} />
        <Route component={NotFound} />
      </Switch>
    </div>
  </div>
);

export default App;
