// Librairies
import React from 'react';
import PropTypes from 'prop-types';

// Render
const withLazyLoading = (
  getComponent,
  Spinner = null,
  onError = () => {}
) => (
  class LazyLoadingWrapper extends React.Component {

    static defaultProps = {
      autoPlay: false,
      maxLoops: 10,
    };

    static propTypes = {
      onLoadingStart: PropTypes.bool.isRequired,
      onLoadingEnd: PropTypes.number.isRequired,
      onError: PropTypes.string.isRequired
    };

    state = {
      Component: null
    };

    componentWillMount() {
      const { onLoadingStart, onLoadingEnd, onError } = this.props;
      // onLoadingStart();

      // Before the wrapper component mounts we fire importer function
      getComponent()
        .then((esModule) => {
          // and store the component in state
          this.setState({ Component: esModule.default });
        })
        .catch((err) => {
          onError(err, this.props);
        });
    }

    render() {
      const { Component } = this.state;

      if (!Component) return Spinner;

      return <Component {...this.props} />;
    }

  }
);

export default withLazyLoading;
