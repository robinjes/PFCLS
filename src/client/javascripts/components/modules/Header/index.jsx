// Librairies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// Styles
import './style.css';
// Render
class Header extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <header className="header"><h1>PFCLS</h1></header>
    );
  }
}

export default Header;
