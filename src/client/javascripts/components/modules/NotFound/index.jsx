// Librairies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Styles
import './styles.css';

// Render
const NotFound = (props) => {

  const className = props.className ? props.className + ' notFound' : 'notFound';

  return (
    <div className="notFound">
      <h1>Oops!</h1>
    </div>
  );
};

NotFound.propTypes = {
  className: PropTypes.string
};

NotFound.defaultProps = {
  className: 'notfound'
};

export default NotFound;
