// Librairies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
// HOC
import withLazyLoading from 'components/hoc/withLazyLoading';
// Components
import Loader from 'components/atoms/Loader';
import Challengers from 'components/organisms/Challengers/lazy';
import Board from 'components/organisms/Board';
// Constantes
import { WORDING_WAIT } from 'constantes/wordings';
// Styles
import './style.css';
// Init Socket.io
window.socket = window.socket || io();

// Render
class Room extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: this.props.user || {},
      opponent: {},
      countdown: null,
      hasError: false
    };

    this.handleNewChallenger = this.handleNewChallenger.bind(this);
    this.handleSelectChallenger = this.handleSelectChallenger.bind(this);
    this.handlePlayerChoice = this.handlePlayerChoice.bind(this);
    this.handlePlay = this.handlePlay.bind(this);
  }

  componentDidMount() {
    // Got User online.
    window.socket.on('message', (data) => {
      this.props.setOnlineStatus(true);
      this.props.setUser({ id: data.id });
    });
    // Got a new challengers list.
    window.socket.on('update challengers', (message) => {
      this.props.setChallengers(message.challengers);
    });
    // Got a winner
    window.socket.on('winner is', (message) => {
      if (message.winner) {
        if (message.winner === this.props.user.id) {
          return console.log('You won!', message.phrase);
        }
        return console.log('You loose!', message.phrase);
      }
      return console.log(message.phrase);
    });
    // Got times out.
    window.socket.on('times out', (data) => {
      this.props.setUser({ gaming: false });
      this.setState({ countdown: null });
    });
    // Got a countdown.
    window.socket.on('countdown', (message) => {
      if (message.start) {
        this.props.setUser({ gaming: true });
      }
      this.setState({ countdown: message.counter });
    });
  }

  componentWillReceiveProps(nextProps) {
    const currentStateUser = this.state.user;
    const nextUser = nextProps.room.challengers[currentStateUser.id] || null;
    // Set/Update opponent state
    if (nextUser && nextUser.opponent) {
      this.setState({ opponent: nextProps.room.challengers[nextUser.opponent] || null });
    }
    // Set user state
    if (!currentStateUser.id) {
      this.setState({ user: nextProps.user, countdown: null });
    }
    // Update user state
    if (nextUser && nextProps.user && nextProps.user.id) {
      this.setState({ user: nextUser });
    }
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  handleNewChallenger(name) {
    window.socket.emit(
      'new challenger set',
      Object.assign({}, this.state.user, { username: name })
    );
  }

  handlePlayerChoice(evt) {
    const choice = evt.target.dataset.card;
    if (choice) {
      window.socket.emit(
        'player shows card',
        { player: this.props.room.challengers[this.state.user.id], choice }
      );
    }
  }

  handleSelectChallenger(id) {
    window.socket.emit('challenge on', { players: [this.state.user.id, id] });
  }

  handlePlay() {
    this.setState({ gaming: WORDING_WAIT });
    window.socket.emit('ready to play');
    this.props.setUser({ gaming: true });
  }

  render() {
    return (
      <div className="room">
        {this.state.user && this.state.user.username && this.state.user.playing &&
          <Board
            countdown={this.state.countdown}
            player={this.state.user.username}
            opponent={this.state.opponent.username}
            gaming={this.state.user.gaming}
            selectCardFn={this.handlePlayerChoice}
            onPlayFn={this.handlePlay}
          />
        }
        {this.state.user.id && !this.state.user.playing &&
          <Challengers
            userid={this.state.user.id}
            username={this.state.user.username}
            challengers={this.props.room.challengers}
            onNewChallenger={this.handleNewChallenger}
            onSelectChallenger={this.handleSelectChallenger}
          />
        }
      </div>
    );
  }

}

Room.propTypes = {
  user: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
      PropTypes.bool
    ])
  ),
  room: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
    ]))
  ]),
  setOnlineStatus: PropTypes.func.isRequired,
  setUser: PropTypes.func.isRequired,
  setChallengers: PropTypes.func.isRequired
};

Room.defaultProps = {
  user: null,
  room: {}
};

export default Room;
