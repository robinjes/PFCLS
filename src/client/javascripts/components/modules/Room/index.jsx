// Librairies
import { connect } from 'react-redux';
// Actions
import {
  setUser,
  setChallengers,
  setOnlineStatus
} from 'actions';
// Components
import Room from './Room';

// Maps
const mapStateToProps = (state, ownProps) => ({
    user: state.user,
    room: state.room
  }),
  mapDispatchToProps = dispatch => ({
    setUser: user => (
      dispatch(setUser(user))
    ),
    setChallengers: id => (
      dispatch(setChallengers(id))
    ),
    setOnlineStatus: status => (
      dispatch(setOnlineStatus(status))
    )
  }),
  // Connect
  RoomConnector = connect(
    mapStateToProps,
    mapDispatchToProps
  )(Room);

export default RoomConnector;
