// Librairies
import React from 'react';
import PropTypes from 'prop-types';
// Components
import Button from 'components/atoms/Button';
import Loader from 'components/atoms/Loader';
// Constantes
import { WORDING_WAIT } from 'constantes/wordings';
// Styles
import './styles.css';

const Board = ({ player, opponent, gaming, countdown, selectCardFn, onPlayFn }) => (
  <div className="board">
    <div className="board-players mg-auto">
      <div
        className="board-players__player inline-block pg-t-m pg-r-xs pg-b-m pg-l-m"
      >{player}</div>
      <div className="board-players__opponent inline-block pg-t-m pg-r-m pg-b-m pg-l-xs">
        vs. {opponent}
      </div>
    </div>
    <div className="c-middle mg-t-xxl">Ready ?</div>
    {gaming &&
      <div className="flex-row-wrap mg-t-xxl">
        <div
          tabIndex="0"
          role="button"
          className="card flex-2-clmns flex-center mg-s"
          onClick={selectCardFn}
          data-card="lezard"
        ><p>Lézard</p></div>
        <div
          tabIndex="0"
          role="button"
          className="card flex-2-clmns flex-center mg-s"
          onClick={selectCardFn}
          data-card="spock"
        ><p>Spock</p></div>
        <div
          tabIndex="0"
          role="button"
          className="card flex-2-clmns flex-center mg-s"
          onClick={selectCardFn}
          data-card="ciseaux"
        ><p>Ciseaux</p></div>
        <div
          tabIndex="0"
          role="button"
          className="card flex-2-clmns flex-center mg-s"
          onClick={selectCardFn}
          data-card="feuille"
        ><p>Feuille</p></div>
        <div
          tabIndex="0"
          role="button"
          className="card flex-2-clmns flex-center mg-s"
          onClick={selectCardFn}
          data-card="pierre"
        ><p>Pierre</p></div>
      </div>
    }
    {gaming === WORDING_WAIT &&
      <div>
        <Loader />
        <p className="c-middle">We are now waiting for the Opponent to be ready...</p>
      </div>
    }
    {!gaming &&
      <Button className="mg-t-xxl" onClick={onPlayFn}>Go!</Button>
    }
    {gaming && countdown &&
      <div><b>{countdown}</b></div>
    }
  </div>
);

Board.propTypes = {
  player: PropTypes.string.isRequired,
  opponent: PropTypes.string.isRequired,
  gaming: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string
  ]),
  countdown: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object
  ]),
  selectCardFn: PropTypes.func,
  onPlayFn: PropTypes.func
};

Board.defaultProps = {
  gaming: false,
  countdown: null,
  selectCardFn: () => {},
  onPlayFn: () => {}
};

export default Board;
