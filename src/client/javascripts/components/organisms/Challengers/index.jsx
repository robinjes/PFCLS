// Librairies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Components
import Field from 'components/atoms/Field';
import Button from 'components/atoms/Button';
import Loader from 'components/atoms/Loader';
// Styles
import './styles.css';

class Challengers extends Component {

  constructor(props) {
    super(props);

    this.state = {
      username: this.props.username || ''
    };

    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleOnValidate = this.handleOnValidate.bind(this);
    this.handleOnSelect = this.handleOnSelect.bind(this);
  }

  handleOnChange(evt) {
    this.setState({ username: evt.target.value });
  }

  handleOnValidate(evt) {
    if ((evt.type === 'keypress' && evt.key === 'Enter') || evt.type === 'click') {
      this.props.onNewChallenger(this.state.username);
    }
  }

  handleOnSelect(evt) {
    this.props.onSelectChallenger(evt.target.id);
  }

  render() {
    return (
      <div className="challengers">
        {!this.props.username &&
          <div className="initPlayer">
            <Field
              id="challenger-user__name"
              className="algn-c"
              placeholder="Player name"
              onChange={this.handleOnChange}
              onKeyPress={this.handleOnValidate}
            />
            <Button className="mg-t-xxl" onClick={this.handleOnValidate}>Go!</Button>
          </div>
        }
        {this.props.username && Object.keys(this.props.challengers).length > 1 &&
          <div>
            <h2>{this.props.username}</h2>
            <p className="c-middle">Choose your challenger</p>
            <div className="challengers-items flex-row-wrap">
              {Object.keys(this.props.challengers).map(key => (
                (() => {
                  if (this.props.challengers[key].id !== this.props.userid) {
                    const isPlaying = this.props.challengers[key].playing ? ' - Busy' : '';
                    return (
                      <div
                        className="challenger-items_item flex-2-clmns flex-center mg-s"
                        key={this.props.challengers[key].id}
                      >
                        <div
                          id={this.props.challengers[key].id}
                          tabIndex="0"
                          role="button"
                          onClick={this.handleOnSelect}
                        >
                          {this.props.challengers[key].username}{isPlaying}
                        </div>
                      </div>
                    );
                  }
                  return null;
                })()
              ))}
            </div>
          </div>
        }
        {this.props.username && Object.keys(this.props.challengers).length <= 1 &&
          <div>
            <h2>{this.props.username}</h2>
            <Loader />
            <p className="c-middle">We are now waiting for Challengers...</p>
          </div>
        }
      </div>
    );
  }
}

Challengers.propTypes = {
  username: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  userid: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  challengers: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
    ]))
  ]),
  onNewChallenger: PropTypes.func.isRequired,
  onSelectChallenger: PropTypes.func.isRequired
};

Challengers.defaultProps = {
  username: null,
  userid: null,
  challengers: {}
};

export default Challengers;
