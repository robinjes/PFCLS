// Librairies
import React, { Component } from 'react';
// Components
import Loader from 'components/atoms/Loader';
// HOC
import withLazyLoading from 'components/hoc/withLazyLoading';

/* eslint-disable */
export default withLazyLoading(
  () => System.import(/* webpackChunkName: "Post" */ './'),
  <Loader type="spinner" />
);
/* eslint-enable */
