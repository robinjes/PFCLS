const WORDING_WAIT = 'WAITING';
const WORDING_UNKWN = 'UNKNOWN';

export default { WORDING_WAIT, WORDING_UNKWN };
