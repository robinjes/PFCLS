import createReducer from 'utils/createReducer';

/* Initial state */
const initialState = {
    online: false
  },
  /* Application reducer */
  application = {
    SET_ONLINE_STATUS: (state, action) => (
      Object.assign({}, state, {
        online: action.online
      })
    ),
    // For potential error management
    FAILED: (state, action) => {
      const newMessages = state.items || [];
      newMessages.push(action.message);
      return Object.assign({}, state, {
        messages: newMessages
      });
    }
  };

export default createReducer(initialState, application);
