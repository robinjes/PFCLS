import createReducer from 'utils/createReducer';

/* Initial state */
const initialState = {},
  /* Application reducer */
  board = {
    SET_OPPONENT: (state, action) => {
      if (action.opponent) {
        const opponent = Object.assign({}, state.opponent, action.opponent);
        return Object.assign({}, state, {
          opponent
        });
      }
      return state;
    }
  };

export default createReducer(initialState, board);
