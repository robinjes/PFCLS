import { combineReducers } from 'redux';
import application from './application';
import room from './room';
import user from './user';
import board from './board';

const app = combineReducers({
  application,
  room,
  user,
  board
});
export default app;
