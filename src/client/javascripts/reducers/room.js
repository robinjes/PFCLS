import createReducer from 'utils/createReducer';

/* Initial state */
const initialState = {
    challengers: {}
  },
  /* Room reducer */
  room = {
    ADD_CHALLENGER: (state, action) => {
      const challengers = state.challengers;
      const challenger = action.challenger;
      challengers[challenger.id] = challenger;
      return Object.assign({}, state, {
        challengers
      });
    },
    REMOVE_CHALLENGER: (state, action) => {
      const challengers = state.challengers;
      delete challengers[action.id];
      return Object.assign({}, state, {
        challengers
      });
    },
    SET_CHALLENGERS: (state, action) => (
      Object.assign({}, state, {
        challengers: action.challengers
      })
    )
  };

export default createReducer(initialState, room);
