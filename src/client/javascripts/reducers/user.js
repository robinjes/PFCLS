import createReducer from 'utils/createReducer';

/* Initial state */
const initialState = {},
  /* Application reducer */
  user = {
    SET_USER: (state, action) => (
      Object.assign({}, state, action.user)
    )
  };

export default createReducer(initialState, user);
