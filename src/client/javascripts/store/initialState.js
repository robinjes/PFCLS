/* Initial states */
import { initialState as applicationInitState } from 'reducers/application';
import { initialState as roomInitState } from 'reducers/room';
import { initialState as userInitState } from 'reducers/user';

const initialState = {
  application: applicationInitState,
  room: roomInitState,
  user: userInitState
};

export default initialState;
