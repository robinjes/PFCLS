const createReducer = (initialState, handlers) => (
  (state = initialState, action = null) => {
    const handler = handlers[action.type];
    if (!handler) {
      return state;
    }
    return handler(state, action);
  }
);

export default createReducer;
