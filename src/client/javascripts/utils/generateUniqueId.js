const generateUniqueId = () => (
  new Date().getTime()
);

export default generateUniqueId;