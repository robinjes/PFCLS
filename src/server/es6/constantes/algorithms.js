const figures = {};
// Lézard
figures.lezard = {
  name: 'le lezard',
  beats: new Map([
    ['spock', 'empoisonne'],
    ['feuille', 'mange']
  ])
};
// Spock
figures.spock = {
  name: 'spock',
  beats: new Map([
    ['ciseaux', 'casse'],
    ['pierre', 'vaporise']
  ])
};
// Ciseaux
figures.ciseaux = {
  name: 'les ciseaux',
  beats: new Map([
    ['lezard', 'décapite'],
    ['feuille', 'coupe']
  ])
};
// Feuille
figures.feuille = {
  name: 'la feuille',
  beats: new Map([
    ['pierre', 'recouvre'],
    ['spock', 'discrédite']
  ])
};
// Pierre
figures.pierre = {
  name: 'la pierre',
  beats: new Map([
    ['lezard', 'écrase'],
    ['ciseaux', 'émousse']
  ])
};

// figures.lezard.beats.get("spock");  // "empoisonne"
// figures.lezard.beats.has("spock");  // true
// figures.lezard.beats.has("pierre");  // false

export default figures;
