// Librairies
import express from 'express';
import helmet from 'helmet';
// Shared
import {
  setChallenger,
  removeChallenger,
  setOpponents,
  removeOpponents,
  setChallenge,
  removeChallenge,
  setChallengePlayerChoice,
  resetChallengePlayersChoices,
  arePlayersReadyForResult,
  getWinner,
  getChallengeId
} from './shared/challenge_manager';
// Constantes
import figures from './constantes/algorithms';

// Data storage
let challengers = {};

let challenges = {};

const countdowns = {};

// Express
const app = express();
app.use(helmet());
app.use('/', express.static('public'));

// SockeIO
const io = require('socket.io').listen(app.listen(process.env.PORT || 3000));

io.on('connection', (client) => {

  // Connected to server
  console.log(`New challenger online: ${client.id}`);
  client.emit('message', { id: client.id, figures });

  // Disconnected to server
  client.on('disconnect', (reason) => {
    console.log(`Challenger ${client.id} left.`);
    // If challenger is part of challengers list
    if (challengers[client.id]) {
      const challenger = challengers[client.id];
      // get the challenge ID
      getChallengeId(challenges, challenger)
        .then((challengeId) => {
          const players = [client.id, challengers[client.id].opponent];
          // challengers leave the room
          if (io.sockets.connected[players[0]]) {
            io.sockets.connected[players[0]].leave(challengeId);
          }
          if (io.sockets.connected[players[1]]) {
            io.sockets.connected[players[1]].leave(challengeId);
          }
          removeChallenge(challenges, players)
            .then((newChallengesList) => {
              // Remove opponents of each challengers
              removeOpponents(challengers, challengers[client.id])
                .then((newChallengersListA) => {
                  // Remove challenger from the list of challengers
                  removeChallenger(newChallengersListA, { id: client.id })
                    .then((newChallengersListB) => {
                      challenges = newChallengesList;
                      challengers = newChallengersListB;
                      // clear countdown of challenge
                      clearInterval(countdowns[challengeId]);
                      io.emit('update challengers', { message: reason, id: client.id, challengers: newChallengersListB });
                    })
                    .catch((err) => {
                      console.error('Error: ', err.message);
                    });
                })
                .catch((err) => {
                  console.error('Error: ', err.message);
                });
            })
            .catch((err) => {
              console.error('Error: ', err.message);
            });
        })
        .catch((err) => {
          console.error('Error : ', err.message);
        });
    }
  });

  // Send message to everyone but the sender
  client.on('new challenger set', (user) => {
    console.log('New challenger set');
    setChallenger(challengers, user)
      .then((res) => {
        challengers = res;
        // everyone gets it but the sender
        io.emit('update challengers', { id: user.id, challengers: res });
      })
      .catch((err) => {
        console.error('Error: ', err.message);
      });
  });

  client.on('challenge on', (data) => {
    console.log('Challenge on');
    setOpponents(challengers, data.players)
      .then((resA) => {
        const message = `${data.players[0]} and ${data.players[1]} started playing.`;
        setChallenge(challenges, data.players)
          .then((resB) => {
            challengers = resA;
            challenges = resB.challenges;
            const challengeId = resB.id;
            // challengers join a room
            io.sockets.connected[data.players[0]].join(challengeId);
            io.sockets.connected[data.players[1]].join(challengeId);
            // everyone gets it but the sender
            io.emit('update challengers', { message, challengers: resA });
          })
          .catch((err) => {
            console.error('Error: ', err.message);
          });
      })
      .catch((err) => {
        console.error('Error: ', err.message);
      });
  });

  client.on('ready to play', (data) => {
    console.log('ready to play');
    setChallenger(challengers, { id: client.id }, { ready: true })
      .then((resA) => {
        challengers = resA;
        const challenger = challengers[client.id];
        const opponent = challengers[challenger.opponent];
        if (challenger && opponent) {
          if (challenger.ready && opponent.ready) {
            console.log('Challengers ready to play!');
            getChallengeId(challenges, { id: challenger.id, opponent: challenger.opponent })
              .then((id) => {
                let counter = 3;
                io.in(id).emit('countdown', { start: true, counter });
                countdowns[id] = setInterval(() => {
                  counter -= 1;
                  if (counter === 0) {
                    io.in(id).emit('times out', { message: 'Time\'s out!' });
                    setChallenger(challengers, { id: challenger.id }, { ready: null })
                      .then((resB) => {
                        challengers = resB;
                        setChallenger(challengers, { id: opponent.id }, { ready: null })
                          .then((resC) => {
                            challengers = resC;
                          })
                          .catch((err) => {
                            console.error('Error: ', err.message);
                          });
                      })
                      .catch((err) => {
                        console.error('Error: ', err.message);
                      });
                    return clearInterval(countdowns[id]);
                  }
                  return io.in(id).emit('countdown', { counter });
                }, 1000);
              })
              .catch((err) => {
                console.error('Error: ', err.message);
              });
          }
        }
      })
      .catch((err) => {
        console.error('Error: ', err.message);
      });
  });

  client.on('challenge off', (data) => {
    console.log('Challenge off');
    removeOpponents(challengers, challengers[data.player.id])
      .then((resA) => {
        const players = [data.player.id, challengers[data.player.id].opponent];
        removeChallenge(challenges, players)
          .then((resB) => {
            challenges = resB;
            challengers = resA;
            const message = `${challengers[data.player.username]} left the challenge.`;
            // clear countdown of challenge
            clearInterval(countdowns[resB.id]);
            // everyone gets it but the sender
            io.emit('update challengers', { message, challengers: resA });
          })
          .catch((err) => {
            console.error('Error: ', err.message);
          });
      })
      .catch((err) => {
        console.error('Error: ', err.message);
      });
  });

  client.on('player shows card', (data) => {
    console.log('Card shown.');
    return setChallengePlayerChoice(challenges, data.player, data.choice)
      .then((resA) => {
        challenges = resA;
        arePlayersReadyForResult(challenges, data.player)
          .then((resB) => {
            if (resB.shown) {
              getWinner(challenges, resB.challengeId, figures)
                .then((resC) => {
                  // reset players' choices
                  resetChallengePlayersChoices(challenges, resB.challengeId)
                    .then((resD) => {
                      challenges = resD;
                      // clear countdown of challenge
                      clearInterval(countdowns[resB.challengeId]);
                      // to challengers/game (room)
                      io.in(resB.challengeId).emit('winner is', resC);
                    })
                    .catch((err) => {
                      console.error('Error: ', err.message);
                    });
                })
                .catch((err) => {
                  console.error('Error: ', err.message);
                });
            }
            return resB.shown;
          })
          .catch((err) => {
            console.error('Error: ', err.message);
          });
      })
      .catch((err) => {
        console.error('Error: ', err.message);
      });
  });
});
