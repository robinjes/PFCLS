const setChallenger = (challengersList, challenger = {}, extraProps = {}) => (
  new Promise((resolve, reject) => {
    if (challengersList && challenger) {
      const challengersCopy = challengersList;
      const currentChallenger = challengersCopy[challenger.id] || {};
      challengersCopy[challenger.id] = Object.assign(
        {},
        Object.assign({}, currentChallenger, {
          id: challenger.id,
          username: challenger.username,
          playing: challenger.playing || currentChallenger.playing || false
        }),
        extraProps
      );
      return resolve(challengersCopy);
    }
    return reject({ message: 'setChallenger - Missing parameter(s).' });
  })
);

const removeChallenger = (challengersList, challenger) => (
  new Promise((resolve, reject) => {
    if (challengersList && challenger) {
      const challengersCopy = challengersList;
      delete challengersCopy[challenger.id];
      return resolve(challengersCopy);
    }
    return reject({ message: 'removeChallenger - Missing parameter(s).' });
  })
);

const setOpponents = (challengersList, players) => (
  new Promise((resolve, reject) => {
    if (challengersList && players) {
      const challengersCopy = challengersList;
      const playerOne = challengersCopy[players[0]];
      const playerTwo = challengersCopy[players[1]];
      if (playerOne && playerTwo) {
        challengersCopy[playerOne.id].playing = true;
        challengersCopy[playerOne.id].opponent = playerTwo.id;
        challengersCopy[playerTwo.id].playing = true;
        challengersCopy[playerTwo.id].opponent = playerOne.id;
        return resolve(challengersCopy);
      }
      return reject({ message: 'setOpponents - Missing player(s).' });
    }
    return reject({ message: 'setOpponents - Missing parameter(s).' });
  })
);

const removeOpponents = (challengersList, player) => (
  new Promise((resolve, reject) => {
    if (challengersList) {
      const challengersCopy = challengersList;
      if (player && player.opponent) {
        const opponentId = challengersCopy[player.id].opponent;
        challengersCopy[player.id].playing = false;
        challengersCopy[opponentId].playing = false;
        delete challengersCopy[player.id].opponent;
        delete challengersCopy[opponentId].opponent;
        return resolve(challengersCopy);
      }
      return resolve(challengersCopy);
    }
    return reject({ message: 'removeOpponents - Missing parameter(s).' });
  })
);

const setChallenge = (challenges, players) => (
  new Promise((resolve, reject) => {
    if (challenges && players && players[0] && players[1]) {
      const challengesCopy = challenges;
      challengesCopy[players[0]] = {
        [players[0]]: { id: players[0] },
        [players[1]]: { id: players[1] }
      };
      return resolve({ challenges: challengesCopy, id: players[0] });
    }
    return reject({ message: 'setChallenge - Missing parameter(s).' });
  })
);

const removeChallenge = (challenges, players) => (
  new Promise((resolve, reject) => {
    if (challenges && players) {
      const challengesCopy = challenges;
      delete challengesCopy[players[0]];
      delete challengesCopy[players[1]];
      return resolve(challengesCopy);
    }
    return reject({ message: 'removeChallenge - Missing parameter(s).' });
  })
);

const setChallengePlayerChoice = (challenges, player, choice) => (
  new Promise((resolve, reject) => {
    if (challenges && player && choice) {
      const challengesCopy = challenges;
      if (challengesCopy[player.id]) {
        challengesCopy[player.id][player.id].choice = choice;
        return resolve(challengesCopy);
      }
      if (challengesCopy[player.opponent]) {
        challengesCopy[player.opponent][player.id].choice = choice;
        return resolve(challengesCopy);
      }
      return reject({ message: 'setChallengePlayerChoice - No challenge found).' });
    }
    return reject({ message: 'setChallengePlayerChoice - Missing parameter(s).' });
  })
);

const resetChallengePlayersChoices = (challenges, challengeId) => (
  new Promise((resolve, reject) => {
    if (challengeId) {
      const challengesCopy = challenges;
      if (challengesCopy[challengeId]) {
        delete challengesCopy[challengeId][Object.keys(challengesCopy[challengeId])[0]].choice;
        delete challengesCopy[challengeId][Object.keys(challengesCopy[challengeId])[1]].choice;
        delete challengesCopy[challengeId][Object.keys(challengesCopy[challengeId])[0]].ready;
        delete challengesCopy[challengeId][Object.keys(challengesCopy[challengeId])[1]].ready;
        resolve(challengesCopy);
      }
      return reject({ message: 'resetChallengePlayersChoices - No challenge found.' });
    }
    return reject({ message: 'resetChallengePlayersChoices - Missing parameter(s).' });
  })
);


const arePlayersReadyForResult = (challenges, player) => (
  new Promise((resolve, reject) => {
    if (challenges && player) {
      const challengesCopy = challenges;
      const playerId = player.id;
      const opponentId = player.opponent;
      const challenger = challengesCopy[playerId];
      const opponent = challengesCopy[opponentId];
      if (challenger || opponent) {
        // looking for the challenge
        if (challenger) {
          if (challenger[playerId].choice && challenger[opponentId].choice) {
            return resolve({
              challengeId: playerId,
              shown: true
            });
          }
          return resolve({ message: 'Missing one challenger choice.', shown: false });
        }
        // looking for the challenge
        if (opponent[playerId].choice && opponent[opponentId].choice) {
          return resolve({
            challengeId: opponentId,
            shown: true
          });
        }
        return resolve({ message: 'Missing one challenger choice.', shown: false });
      }
      return reject({ message: 'arePlayersReadyForResult - No challenge set.' });
    }
    return reject({ message: 'arePlayersReadyForResult - Missing parameter(s).' });
  })
);

const getWinner = (challenges, challengeId, algo) => (
  new Promise((resolve, reject) => {
    if (challenges && challengeId && algo) {
      if (challenges[challengeId]) {
        const player = {
          id: challenges[challengeId][Object.keys(challenges[challengeId])[0]].id,
          choice: challenges[challengeId][Object.keys(challenges[challengeId])[0]].choice,
        };
        const opponent = {
          id: challenges[challengeId][Object.keys(challenges[challengeId])[1]].id,
          choice: challenges[challengeId][Object.keys(challenges[challengeId])[1]].choice,
        };
        if (player && opponent) {
          const playerFigure = algo[player.choice];
          const opponentFigure = algo[opponent.choice];
          if (playerFigure) {
            if (playerFigure.beats.has(opponent.choice)) {
              const winnerName = `${playerFigure.name[0].toUpperCase()}${playerFigure.name.substring(1)}`;
              return resolve({
                winner: player.id,
                phrase: `${winnerName} ${playerFigure.beats.get(opponent.choice)} ${opponentFigure.name}!`
              });
            }
            if (opponentFigure.beats.has(player.choice)) {
              const winnerName = `${opponentFigure.name[0].toUpperCase()}${opponentFigure.name.substring(1)}`;
              return resolve({
                winner: opponent.id,
                phrase: `${winnerName} ${opponentFigure.beats.get(player.choice)} ${playerFigure.name}!`
              });
            }
            return resolve({
              winner: null,
              phrase: 'Drawn! Play again!'
            });
          }
          return reject({ message: 'Choice made doesn\'t exist.' });
        }
        return reject({ message: 'Missing opponent(s) in the challenge.' });
      }
      return reject({ message: 'No challenge set for the given ID.' });
    }
    return reject({ message: 'getWinner - Missing parameter(s).' });
  })
);

const getChallengeId = (challenges, player) => (
  new Promise((resolve, reject) => {
    if (challenges && player) {
      const challengesCopy = challenges;
      const challenge = challengesCopy[player.id] || challengesCopy[player.opponent];
      const challengeId = Object.keys(challengesCopy)[0];
      if (challenge && challengeId) {
        return resolve(challengeId);
      }
      return reject({ message: 'No challenge set.' });
    }
    return reject({ message: 'setChallengePlayerChoice - Missing parameter(s).' });
  })
);

export {
  setChallenger,
  removeChallenger,
  setOpponents,
  removeOpponents,
  setChallenge,
  removeChallenge,
  resetChallengePlayersChoices,
  setChallengePlayerChoice,
  arePlayersReadyForResult,
  getWinner,
  getChallengeId
};
