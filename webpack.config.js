const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const nodeEnv = process.env.NODE_ENV || 'development';
const isProd = nodeEnv === 'production';

const sourcePath = path.join(__dirname, 'src/client/');
const publicPath = path.join(__dirname, 'public');

const extractCSS = new ExtractTextPlugin({ filename: '[name].bundle.css', disable: false, allChunks: true });

const plugins = [
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: Infinity,
    filename: 'vendor.bundle.js'
  }),
  new webpack.DefinePlugin({
    'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
  }),
  new HtmlWebpackPlugin({
    template: sourcePath + '/template/main.template.html',
    production: isProd,
    inject: true,
  }),
];

const jsEntry = [
  sourcePath + '/index'
];

if (isProd) {
  plugins.push(
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    // Minimize all JavaScript output of chunks
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
      },
      output: {
        comments: false
      },
    }),
    extractCSS,
    new CleanWebpackPlugin(['scripts', 'stylesheets', 'images'], {
      root: publicPath,
      verbose: true,
    })
  );

  jsEntry.unshift(
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server'
  );
} else {
  plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  );
}

module.exports = {
  devtool: isProd ? 'source-map' : 'cheap-module-source-map',
  context: sourcePath,
  entry: {
    js: jsEntry,
    vendor: [
      'react',
      'react-dom',
      'redux',
      'react-redux',
      'react-router-dom',
      'redux-thunk',
      'socket.io-client'
    ]
  },
  target: 'web',
  output: {
    path: publicPath,
    filename: 'scripts/[name].[chunkhash].js',
    chunkFilename: '[chunkhash].js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        // ESLint
        test: /\.(js|jsx)$/,
        enforce: 'pre',
        use: {
          loader: 'eslint-loader',
          options: {
            configFile: '.eslintrc',
            include: sourcePath,
            exclude: /node_modules/,
            quiet: true, // to allow 'FriendlyErrorsWebpackPlugin' to work
          }
        }
      },
      {
        test: /\.css$/,
        use: isProd ?
          extractCSS.extract({
            fallbackLoader: 'style-loader',
            loader: ['css-loader?sourceMap', 'postcss-loader'],
            options: {
              importLoaders: 1
            }
          }) :
          [
            { loader: 'style-loader' },
            { loader: 'css-loader',
              options: {
                alias: { styles: sourcePath + 'styles' },
                sourceMap: true,
              },
            },
            { loader: 'postcss-loader' }
          ]
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            query: {
              cacheDirectory: true
            }
          }
        ]
      }
    ],
  },
  resolve: {
    extensions: ['.json', '.js', '.jsx'],
    modules: [
      sourcePath,
      'node_modules'
    ],
    alias: {
      client: sourcePath,
      javascripts: 'client/javascripts',
      styles: 'client/styles',
      components: 'javascripts/components',
      actions: 'javascripts/actions',
      reducers: 'javascripts/reducers',
      constantes: 'javascripts/constantes',
      store: 'javascripts/store',
      utils: 'javascripts/utils'
    }
  },
  plugins
};
